#gpg support
#source /etc/Muttrc.gpg.dist

#set at start
set config_charset=utf-8
set signature="iconv -f utf-8 ~/.signature |"
#set locale=`echo "${LC_ALL:-${LC_TIME:-${LANG}}}"`
#set file_charset="utf-8:iso-8859-1"

# configuration de base
set realname = "Sean V Kelley"
set header_cache =~/.mutt/cache/headers
set certificate_file =~/.mutt/certificates
set message_cachedir =~/.mutt/cache/bodies

# configuration imap
set spoolfile=imaps://proxy.jf.intel.com:993/INBOX
set imap_user = "sean.v.kelley@intel.com"
set imap_pass = ""
set imap_passive="yes"
set imap_check_subscribed="yes"
set imap_list_subscribed="yes"
unset ssl_verify_host

# configuration SMTP
set smtp_url = "smtp://sean.v.kelley@intel.com@proxy.jf.intel.com:587/"
set smtp_pass = ""
set ssl_starttls="yes"
set from = "sean.v.kelley@intel.com"
set use_envelope_from=yes # Pour que postfix ou sendmail ne change pas votre from

# petit plus
set trash = "imaps://proxy.jf.intel.com/[Gmail]/Trash"
set postponed="+[Gmail]/Draft"
set folder = "imaps://proxy.jf.intel.com:993/"
set imap_check_subscribed
set mail_check = 120
set timeout = 300
set imap_keepalive = 300
set signature='/signature.txt'

# Dictionary
set ispell="aspell -e -c"

# Format de date
set date_format="%A %d %b %Y à %H:%M:%S (%Z)"

# phrase d'intro pour réponse quotée
set attribution="Le %d, %n a écrit :"
set forward_format="[Fwd: %s]"
set forward_quote

#
# mailboxes !
mailboxes "="

#
# real delete
#folder-hook . 'unset trash'
#folder-hook imaps://imap.gmail.com 'set trash="=[Gmail]/Trash"'
macro index,pager d "<enter-command>set trash=\"imaps://proxy.jf.intel.com/[Gmail]/Trash\"\n <delete-message>" "Gmail delete message"

#
# real archive
#macro index,pager A "<save-message><enter><sync-mailbox>" "archive"
#set mbox="imaps://imap.gmail.com/[Gmail]/All Mail"
macro index,pager y "<enter-command>unset trash\n <delete-message>" "Gmail archive message"

macro index,pager gi "<change-folder>=INBOX<enter>" "Go to inbox"
macro index,pager ga "<change-folder>=[Gmail]/All Mail<enter>" "Go to all mail"
macro index,pager gs "<change-folder>=[Gmail]/Starred<enter>" "Go to starred messages"
macro index,pager gd "<change-folder>=[Gmail]/Drafts<enter>" "Go to drafts"

#Editeur
set edit_hdrs
auto_view text/html
set auto_tag

set editor="vim" # indiquer ici votre éditeur de texte préféré.

# just scroll one line instead of full page
set menu_scroll=yes

# we want to see some MIME types inline, see below this code listing for explanation
auto_view application/msword
auto_view application/pdf

# make default search pattern to search in To, Cc and Subject
set simple_search="~f %s | ~C %s | ~s %s"

# threading preferences, sort by threads
set sort=threads
set sort_aux='last-date-received'
set strict_threads=yes

# show spam score (from SpamAssassin only) when reading a message
spam "X-Spam-Score: ([0-9\\.]+).*" "SA: %1"
set pager_format = " %C - %[%H:%M] %.20v, %s%* %?H? [%H] ?"

# do not show all headers, just a few
ignore          *
unignore        From To Cc Bcc Date Subject
# and in this order
unhdr_order     *
hdr_order       From: To: Cc: Bcc: Date: Subject:


#uncomment the colorscheme you want to use, and comment out the others
#source $MAILCONF/mutt-colors-solarized/mutt-colors-solarized-dark-16.muttrc
#source ~/.mutt/mutt-colors-solarized/mutt-colors-solarized-light-16.muttrc
#source ~/.mutt/mutt-colors-solarized/mutt-colors-solarized-dark-16.muttrc
#source $MAILCONF/mutt-colors-solarized/mutt-colors-solarized-dark-256.muttrc
#source $MAILCONF/mutt-colors-solarized/mutt-colors-solarized-light-256.muttrc
source ~/.mutt/colors-gruvbox-shuber.muttrc
# For neomutt uncomment this line:
# source ~/.mutt/colors-gruvbox-shuber-extended.muttrc

# Pour la gestion des accents
set locale="en_US.UTF-8"

macro index \cb |urlview\n
macro pager \cb |urlview\n

## Adding a prefix to index mail commands:
## I do this so I can use the letters for pager commands.
## This prevents accindental invocation of the editor.
## I do not use ESC as the prefix key as there needs to be
## a pause between ESC and the following key - annoying.
## I once used the backslash key - but \n, \r, and \t
## are now used for newline, return, and tab.  *sigh*
bind pager ,b   bounce-message
bind pager ,f   forward-message
bind pager ,g   group-reply
bind pager ,m   mail
bind pager ,L   list-reply
bind pager ,n   reply
bind pager ,r   reply
bind pager ,a   reply               # 'a' is for "an
bind pager i    exit

#Google contacts
set query_command="goobook query '%s'"
bind editor <Tab> complete-query
macro index,pager a "<pipe-message>goobook add<return>" "add the sender address to Google contacts"

set pager_index_lines=7
set pager_stop

# set up the sidebar, default not visible
set sidebar_width=18
set sidebar_visible=yes
set sidebar_delim='|'
# set sidebar_sort=yes

# which mailboxes to list in the sidebar
#mailboxes =inbox =ml

# color of folders with new mail
color sidebar_new yellow default

# ctrl-n, ctrl-p to select next, prev folder
# ctrl-o to open selected folder
bind index \CP sidebar-prev
bind index \CN sidebar-next
bind index \CO sidebar-open
bind pager \CP sidebar-prev
bind pager \CN sidebar-next
bind pager \CO sidebar-open

# I don't need these. just for documentation purposes. See below.
# sidebar-scroll-up
# sidebar-scroll-down

# b toggles sidebar visibility
macro index b '<enter-command>toggle sidebar_visible<enter>'
macro pager b '<enter-command>toggle sidebar_visible<enter>'

# Remap bounce-message function to "B"
bind index B bounce-message

# Remap retrieval
bind index "^" imap-fetch-mail


#
# Mario Holbe suggests:
# macro index b '<enter-command>toggle sidebar_visible<enter><refresh>'
# macro pager b '<enter-command>toggle sidebar_visible<enter><redraw-screen>'
#
